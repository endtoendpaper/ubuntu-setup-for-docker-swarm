# Setup Docker Swarm Ubuntu DigitalOcean Instance

## Initial Setup

Create an Ubuntu 22 LTS droplet with DigitalOcean. Select to use “ssh key” for authentication. Link your domain to your DigitalOcean account and then add your domain name to your droplet: https://docs.digitalocean.com/products/networking/dns/how-to/add-domains/ For the ssl certificates, you need both an 'a record' for your-domain.com and www.your-domain.com that points to the IP.

Login as root and clone repo
```
ssh root@your_server_ip
git clone https://gitlab.com/endtoendpaper/ubuntu-setup-for-docker-swarm.git && cd ubuntu-setup-for-docker-swarm
chmod o+x add-user-as-root.sh generate-certificates.sh install-docker.sh regenerate-certificates.sh install-nginx.sh
```

Setup a new user with sudo privileges 
```
./add-user-as-root.sh myuser
```

Logout and then log back in as new user
```
exit
ssh myuser@your_server_ip
```

Clone repo
```
git clone https://gitlab.com/endtoendpaper/ubuntu-setup-for-docker-swarm.git && cd ubuntu-setup-for-docker-swarm
chmod o+x add-user-as-root.sh generate-certificates.sh install-docker.sh regenerate-certificates.sh install-nginx.sh
```

Install Docker
```
sudo ./install-docker.sh
```

Generate Self Signed Certificates for Docker and Init Swarm
```
sudo ./generate-certificates.sh my-domain.com my-IP-addr
```

Install Nginx as a Reverse Proxy and Setup Let's Encrypt SSL Certificates
```
sudo ./install-nginx.sh my-domain.com
```

---

## Other Scripts

Regenerate Self Signed Certificates for Docker If Needed
```
sudo ./regenerate-certificates.sh my-domain.com my-IP-addr
```
