#! /bin/bash
apt update
apt install nginx
touch /etc/nginx/sites-available/$1
echo "server {" >> /etc/nginx/sites-available/$1
echo "    listen 80;" >> /etc/nginx/sites-available/$1
echo "    listen [::]:80;" >> /etc/nginx/sites-available/$1
echo "    server_name $1 www.$1;" >> /etc/nginx/sites-available/$1
echo "    location / {" >> /etc/nginx/sites-available/$1
echo '        proxy_pass http://127.0.0.1:3000;' >> /etc/nginx/sites-available/$1
echo '        proxy_set_header Host $host;' >> /etc/nginx/sites-available/$1
echo '        proxy_set_header X-Real-IP $remote_addr;' >> /etc/nginx/sites-available/$1
echo '        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;' >> /etc/nginx/sites-available/$1
echo '        proxy_set_header X-Forwarded-Proto $scheme;' >> /etc/nginx/sites-available/$1
echo '        proxy_set_header Upgrade $http_upgrade;' >> /etc/nginx/sites-available/$1
echo '        proxy_set_header Connection "upgrade";' >> /etc/nginx/sites-available/$1
echo "    }" >> /etc/nginx/sites-available/$1
echo "    location /cable {" >> /etc/nginx/sites-available/$1
echo '        proxy_pass http://127.0.0.1:3000/cable;' >> /etc/nginx/sites-available/$1
echo '        proxy_http_version 1.1;' >> /etc/nginx/sites-available/$1
echo '        proxy_set_header Upgrade $http_upgrade;' >> /etc/nginx/sites-available/$1
echo '        proxy_set_header Connection "Upgrade";' >> /etc/nginx/sites-available/$1
echo '        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;' >> /etc/nginx/sites-available/$1
echo '        proxy_set_header Host $http_host;' >> /etc/nginx/sites-available/$1
echo '        proxy_set_header X-Real-IP $remote_addr;' >> /etc/nginx/sites-available/$1
echo '        proxy_set_header X-Forwarded-Proto https;' >> /etc/nginx/sites-available/$1
echo '        proxy_redirect off;' >> /etc/nginx/sites-available/$1
echo "    }" >> /etc/nginx/sites-available/$1
echo "}" >> /etc/nginx/sites-available/$1
ln -s /etc/nginx/sites-available/$1 /etc/nginx/sites-enabled/
nginx -t
systemctl restart nginx
apt install certbot python3-certbot-nginx
ufw allow 'Nginx Full'
ufw delete allow 'Nginx HTTP'
ufw allow 'Nginx HTTP'
certbot --nginx -d $1. -d www.$1
systemctl status certbot.timer
certbot renew --dry-run